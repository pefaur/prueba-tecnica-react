const HOST = 'https://swapi.dev';
const CONFIGS = {
    ENV: 'dev',
    BASE_URL: `${HOST}/api`,
};

export default CONFIGS;
