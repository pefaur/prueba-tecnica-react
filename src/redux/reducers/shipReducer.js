import { SELECTED_SHIP, SHIP_LIST } from "../CONSTANTS";

const initialState = {
  loading: false,
  error: false,
  data: []
};

export const shipReducer = (state=initialState, action) => {
  switch (action.type) {
    case SHIP_LIST.LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SHIP_LIST.ADD_ITEM:
      return {
        ...state,
        loading: false,
        data: [...state.data, action.payload],
      };
    case SHIP_LIST.FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload,
      };
    default:
      return state;
  }
};

export const selectedShipReducer = (state=null, action) => {
  switch (action.type) {
    case SELECTED_SHIP:
      return action.payload;
    default:
      return state;
  }
};


