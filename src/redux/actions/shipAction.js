/* eslint-disable import/prefer-default-export */
import _ from 'lodash';
import * as ShipService from '../../services/shipService';
import { SELECTED_SHIP, SHIP_LIST } from '../CONSTANTS';

const getShipAction = (id) => async (dispatch, getState) => {
    try {
        const { starships: { data } } = getState();
        const cached = _.find(data, { id });

        // si el item no está en el store, se consulta al servicio
        if(!cached) {
            // loading
            dispatch({ type: SHIP_LIST.LOADING });
            // get ship for id
            const response = await ShipService.getShip(id);
            // SUCCESS
            dispatch({ type: SHIP_LIST.ADD_ITEM, payload: { ...response, id } });
        } 
        // selected ship
        dispatch({ type: SELECTED_SHIP, payload: id });
    } catch (error) {
        dispatch({ type: SHIP_LIST.FAILURE, payload: error });
    }
};

export {
    getShipAction,
};
