const SHIP_LIST = {
    LOADING: 'SHIP_LIST_LOADING',
    ADD_ITEM: 'SHIP_LIST_ADD_ITEM',
    FAILURE: 'SHIP_LIST_FAILURE',
};
const SELECTED_SHIP = 'SELECTED_SHIP';

export { SHIP_LIST, SELECTED_SHIP }