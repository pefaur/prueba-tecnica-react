import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { Home }  from './pages/Home';
import { ThemeProvider } from '@material-ui/styles';
import store from './redux/store';
import theme from './styles/theme';

const history = createBrowserHistory();

function App() {
    return (
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Router history={history}>
            <Switch>
                  <Route path="/" component={Home} />
                  <Redirect from="*" to="/" />
            </Switch>
          </Router>
        </Provider>
      </ThemeProvider>
    );
}

export default App;