import axios from "axios";
import CONFIGS from "../common/config";

const ShipInterceptor = axios.create({
    baseURL: CONFIGS.BASE_URL,
    headers: {
        Accept: 'application/json',
    },
});

export default ShipInterceptor;