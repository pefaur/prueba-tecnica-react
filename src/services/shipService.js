import ENDPOINTS from "../common/endpoints";
import ShipInterceptor from "./interceptor";

const getShip = async (id) => {
    const response = await ShipInterceptor.get(ENDPOINTS.STARTS_SHIPS + id);
    return response.data;
}

export{ getShip };