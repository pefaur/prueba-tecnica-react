import { FormControl, MenuItem } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import { useState } from 'react';

const initialState = { name: 'Seleccione una nave...', value: 0 };

const ShipSelector = ({ onChange }) => {
    const [ship, setShip] = useState(initialState);

    const handleChange = (select, item) => {
        const { target: { value } } = select;
        const { props: { children }} = item;
        
        setShip({value, name: children});
        onChange(value)
    }

    // Selector Data
    const startShips = [
        { name: 'Executor', value: '15'},
        { name: 'Sentinel-class landing craft', value: '5' },
        { name: 'Death Star', value: '9' },
        { name: 'Millennium Falcon', value: '10' },
        { name: 'Y-wing', value: '11' }
    ];

    return (
        <FormControl fullWidth variant="outlined">
            <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                onChange={handleChange}
                displayEmpty
                value={ship.value}
                renderValue={()=> ship.name}
            >
                {startShips.map((item, key) => (
                    <MenuItem key={key} value={item.value}>{item.name}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export default ShipSelector;