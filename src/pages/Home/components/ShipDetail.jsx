import { Container, Divider, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
    container: {
        background: 'rgba(255, 255, 255, 0.5)',
        color: '#000',
        border: '1px',
        borderStyle: 'solid',
        borderColor: '#fff',
        borderWidth: '1px',
        marginTop: '20px',
        textAlign: 'center',
        padding: '18px',
    },
    h1: {
        fontSize: '36px',
        fontWeight: '700',
        margin: '0px 0px 10px 0px',
    },
    h2: {
        fontSize: '24px',
        fontWeight: '700',
        margin: '18px 0px 0px 0px',
    },
    p: {
        fontSize: '18px',
        fontWeight: '400',
        margin: '5px 0px 10px 0px',
    },
});

const ShipDetail = ({ id, name, model, manufacturer, length, cost_in_credits, passengers }) => {
    const classes = useStyles();
    const renderContainer = () => (
        <>
            <Container className={classes.container} >
                <h1 className={classes.h1}>{name}</h1>
                <p className={classes.p}>{model}</p>
                <Divider />
                <h2 className={classes.h2}>Fabricante</h2>
                <p className={classes.p}>{manufacturer}</p>


                <h2 className={classes.h2}>Largo</h2>
                <p className={classes.p}>{length}</p>

                <h2 className={classes.h2}>Valor</h2>
                <p className={classes.p}>{cost_in_credits}</p>

                <h2 className={classes.h2}>Cantidad pasajeros</h2>
                <p className={classes.p}>{passengers}</p>
            </Container> 
            <Container className={classes.container} >
                <h1 className={classes.h1}>Pasajeros</h1>
                <Divider />
                <p className={classes.p}>Chewbacca</p>
                <p className={classes.p}>Han Solo</p>
                <p className={classes.p}>Lando Calrissian</p>
                <p className={classes.p}>Nien Nunb</p>
            </Container> 
        </>
    )

    return (id ? renderContainer() : <></>)
}

export default ShipDetail;