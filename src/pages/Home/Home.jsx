import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ShipSelector from './components/ShipSelector';
import { Box, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import BgImage from '../../assets/images/bg.jpg'; // Import using relative path
import { getShipAction } from '../../redux/actions/shipAction';
import ShipDetail from './components/ShipDetail';
import _ from 'lodash';

// import { userActions } from '../_actions';

const useStyles = makeStyles(() => ({
    container: {
        backgroundImage: `url(${BgImage})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        height: '100%',
    },
    item: {
        margin: '18px',
        minHeight: '100vh',
    },
    div: {
        backgroundColor: '#ccc',
        width: '100%',
        height: '100%',
    },
}));

const Home = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { starships, selectedShip } = useSelector(state => state);
    const [selected, setSelected] = useState({});

    useEffect(() => {
       console.log('HOME starships:::', starships);
       if(selectedShip){
            const currentShip = _.find(starships.data, { id: selectedShip });
            console.log('SELECTED: ', starships, currentShip, selectedShip);
            setSelected(currentShip);
       }
    }, [selectedShip]);

    // Actualizar nave al cambiar el selector
    const onChange = (id) => dispatch(getShipAction(id));

    return (
        <Box height="100%" className={classes.container}>
            <Grid container>
                <Grid item xs={12} className={classes.item}>
                    <ShipSelector onChange={onChange} />
                    <ShipDetail {...selected} />
                </Grid>
            </Grid>
      </Box>
    );
}

export { Home };