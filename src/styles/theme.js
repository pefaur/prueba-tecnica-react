import { createTheme } from "@material-ui/core";

const theme = createTheme({
    overrides: {
      MuiOutlinedInput: {
        notchedOutline: {
          borderColor: '#fff !important',
          borderWidth: '1px !important',
          borderRadius: '0px',
        },
      },
      MuiSelect: {
        select: {
            backgroundColor: '#000',
            color: '#fff',
            fontSize: '18px',
            fontWeight: '400',
        },
        icon: {
            color: '#fff',
        },
      },
      MuiPaper: {
        root: {
            borderRadius: '0px !important',
            transition: 'transition: opacity 295ms',
        }
      },
      MuiList: {
        root: {
          margin: '1px',
          padding: '0px !important',
        }
      },
      MuiListItem: {
        root: {
            "&$selected": { 
                backgroundColor: "#000",
            },
            "&.Mui-focusVisible": {
                backgroundColor: "#000",
            },
        }
      },
      MuiMenuItem: {
        root: {
          backgroundColor: '#000',
          color: '#fff',
          fontSize: '18px',
          fontWeight: '400',
          "&.Mui-focusVisible": {
            backgroundColor: "#000",
          },
          "&:hover": {
            //you want this to be the same as the backgroundColor above
            backgroundColor: 'rgba(0, 0, 0, 0.86)',
          },
        },
      }
    },
    palette: {
      primary: {
        main: '#fff',
      },
      secondary: {
        main: '#fff',
      },
    },
    props: {
      MuiSelect: {
        MenuProps: {
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "left"
          }
        }
      }
    }
  });

  export default theme;